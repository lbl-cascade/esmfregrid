from distutils.core import setup,Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext
import numpy
import codeVersionID

inc_dirs = []
inc_dirs.append(numpy.get_include())
lib_dirs = []
#lib_dirs.append(numpy.get_lib())
libs = []

cmdclass = {'build_ext': build_ext}

extensions = [Extension("esmfRegrid_cython",["esmfRegrid_cython.pyx"],libraries=libs,library_dirs=lib_dirs,include_dirs=inc_dirs,runtime_library_dirs=lib_dirs,extra_compile_args=["-fopenmp"],extra_link_args=['-fopenmp'])]

#Get the code version ID
cvid = codeVersionID.codeVersionID(__file__)
#Write the REVISION file
cvid.writeRevisionFile()

setup(
            name = 'esmfRegrid', \
            version = cvid.ID, \
            scripts = ['regridFile'], \
            py_modules = ['esmfRegrid','simpleMPI','codeVersionID','regridFile'], \
            cmdclass = cmdclass,\
            ext_modules = extensions, \
            packages = [''], \
            package_data = {'' : ['REVISION']}, \
            )
