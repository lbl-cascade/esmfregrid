.PHONY: default
default: build

PYTHON = python
PYTHON_INCLUDE = ${shell ${PYTHON} -c 'from distutils import sysconfig; print( sysconfig.get_python_inc() )'}
NUMPY_INCLUDE = ${shell ${PYTHON} -c 'import numpy; print( numpy.get_include() )'}

TIMEIT = ${shell ${PYTHON} -c 'import timeit; print(timeit.__file__)'}

CYTHON = cython
src: esmfRegrid_cython.c
esmfRegrid_cython.c: esmfRegrid_cython.pyx
	${CYTHON} -I${NUMPY_INCLUDE} $<


CC = cc -shared -fPIC
CFLAGS = -shared -fPIC
SO = ${shell ${PYTHON} -c 'import imp; print (imp.get_suffixes()[0][0])'}
.PHONY: build
build: esmfRegrid_cython${SO}
esmfRegrid_cython${SO}: esmfRegrid_cython.pyx
	${PYTHON} setup.py build_ext --inplace


.PHONY: test
test: esmfRegrid_cython${SO} esmfRegrid.py
	#${PYTHON} ${TIMEIT} -n2 -r3 "import testTiming as test; test.runTest()"
	${PYTHON} esmfRegrid.py

.PHONY: clean
clean:
	${RM} esmfRegrid_cython.c esmfRegrid_cython${SO}
