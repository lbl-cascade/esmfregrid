#!/usr/bin/env python
import subprocess
import os
import time

class codeVersionID:

  def __init__(self,file=None,gitRepo=True,beVerbose=False):
    """ 
    A module to automatically obtain code version information if possible, and
    the julian time if not.

    Attempts to use 'hg id' to obtain version information.  If that fails, it
    attempts to read version information from a file called REVISION, and
    finally returns the current julian time if all else fails.
    """

    #By default assume that we are using mercurial
    self.reportingFromFile = False
    self.reportingTime = False

    if file is None:
        file = __file__

    pwd = os.getcwd()

    try:
      #Get the file's directory
      fileDir = os.path.dirname(os.path.realpath(file))
      repoDir = fileDir
    except:
      repoDir = "./"

    try:
      #Attempt to get the git revision
      if gitRepo:
          cwd = os.getcwd()
          os.chdir(repoDir) #Change to the repo dir
          gitID = subprocess.check_output('git log -n 1 --pretty=format:"%H" -- {}'.format(fileDir).split(),stderr=subprocess.STDOUT)
          os.chdir(cwd) #Change back to the original directory

          ID=gitID.split(' ')[0][1:-1]
      #Try to get the mercurial revision
      else:
          #First, try to read the version information from mercurial
          mercID=subprocess.check_output(["hg","id",repoDir],stderr=subprocess.STDOUT)
          ID=mercID.split(' ')[0]
    except BaseException as e:

      if beVerbose:
          print(e)
    #Second, try to read the version information from a REVISION file
    #(this would be used if we strip hg information from a tagged repo version
    # and store the version code in the REVISION file)
      try:
        with open("{}/REVISION".format(repoDir),'r') as fin:
          contents = fin.read()
          ID=contents.split(' ')[0]
        self.reportingFromFile = True
      except:
      #Finally, at very worst, return the current timestamp so that 
      #version information might be inferred at a later date
        ID = int(time.time())
        self.reportingTime = True

    #Set the ID property
    self.ID = ID

    cwd = os.getcwd()
    if gitRepo:
        try:
            #First try to read branch information from git
            cwd = os.getcwd()
            os.chdir(repoDir) #Change to the repo dir
            gitBranches = subprocess.check_output('git branch'.split(),stderr=subprocess.STDOUT).split('\n')
            os.chdir(cwd) #Change back to the original directory

            #Parse the current branch from the listing
            #of `git branch`
            branch = None
            for line in gitBranches:
                try:
                    if line[0] == '*':
                        branch = line.split()[1]
                except:
                    pass
        except:
            #Otherwise, set it to nothing
            branch = None

    else:
        try:
        #First try to read branch information from mercurial
          os.chdir(repoDir)
          mercBranch = subprocess.check_output(["hg","branch"],stderr=subprocess.STDOUT)
          os.chdir(cwd)
          branch = mercBranch.split(" ")[0].rstrip()
        except:
        #Otherwise, set it to nothing
          branch = None

    #Finally, try to read the version information from a REVISION file
    #(this would be used if we strip hg information from a tagged repo version
    # and store the version code in the REVISION file)
    if branch is None:
        try:
          with open("{}/REVISION".format(repoDir),'r') as fin:
            contents = fin.read().strip()
            branch=contents.split(' ')[1]
          self.reportingFromFile = True
        except:
        #Otherwise, set it to nothing
          branch = None
        
    #Make sure that we end back up in the original directory
    os.chdir(pwd)

    #Set the branch property
    self.branch = branch

  def writeRevisionFile(self,fileName = 'REVISION'):
      with open(fileName,'w') as fout:
          fout.write('{} {}\n'.format(self.ID,self.branch))

if(__name__ == "__main__"):
  #If run from the command line, simply print the inferred version
  print((codeVersionID(beVerbose=False).ID, codeVersionID(beVerbose=False).branch))

  codeVersionID().writeRevisionFile()
